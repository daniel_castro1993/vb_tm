#!/bin/bash

rm -rf *._
rm -rf ./src/._*
rm -rf ./include/._*
rm -rf ./tests/._*

rm -r build
mkdir build
cd build

cmake .. -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug \
    -DCMAKE_C_FLAGS_DEBUG="-g3 -gdwarf-2" -DCMAKE_CXX_FLAGS_DEBUG="-g3 -gdwarf-2" \
    -DCMAKE_EXPORT_COMPILE_COMMANDS=ON
make clean
make -j8
