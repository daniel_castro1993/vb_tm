#ifndef HS_TM_H
#define HS_TM_H

#include "hs_tm_low_level.h"

#ifdef __cplusplus
extern "C"
{
#endif

// ###################
// Exposed API
// ###################

// TODO: define to enable/disable statistics
extern __thread unsigned long long HS_TM_nb_reads,
                                   HS_TM_nb_writes,
                                   HS_TM_nb_htm_aborts,
                                   HS_TM_nb_htm_commits,
                                   HS_TM_nb_cas,
                                   HS_TM_nb_aborts,
                                   HS_TM_nb_commits;

// updated after exit thread (except sgl)
extern unsigned long long HS_TM_tot_nb_reads,
                          HS_TM_tot_nb_writes,
                          HS_TM_tot_nb_htm_aborts,
                          HS_TM_tot_nb_htm_commits,
                          HS_TM_tot_nb_cas,
                          HS_TM_tot_nb_aborts,
                          HS_TM_tot_nb_commits;

// bootstraps the TM library
void HS_TM_init();
void HS_TM_destroy();

void HS_TM_thr_enter();
void HS_TM_thr_exit();

#define HS_TM_begin() hs_tm_ll_begin()
// re-starts the transaction
#define HS_TM_abort() hs_tm_ll_abort()
#define HS_TM_commit() hs_tm_ll_commit()

#define HS_TM_read(addr, size, buffer)  hs_tm_ll_read(addr, size, buffer)
#define HS_TM_write(addr, size, buffer) hs_tm_ll_write(addr, size, buffer)

#ifdef __cplusplus
}
#endif

#endif /* HS_TM_H */
