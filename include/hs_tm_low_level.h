#ifndef HS_TM_LL_H
#define HS_TM_LL_H

// ###################
// DO NOT INCLUDE THIS HEADER!
//
// This header exposes the internals of hs_tm
// for testing/debugging purposes only.
//
// All functions must start with HS_TM_LL_ in order
// to distinguish from HS_TM_ API.
// ###################

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>

#define PACKED __attribute__ ((packed))

#define SETJMP(env)        setjmp(env)
#define LONGJMP(env, code) longjmp(env, code)

// ###################
// tunable parameters
// ###################

// Locking granularity
#ifndef HS_TM_GRANULARITY
#define HS_TM_GRANULARITY     8
#define HS_TM_LOG2GRANULARITY 3
#endif /* HS_TM_GRANULARITY */

// Locking granularity
#ifndef HS_TM_LOCKTABLESIZE
#define HS_TM_LOCKTABLESIZE 0xFFFF // all bits must be 1
#endif /* HS_TM_LOCKTABLESIZE */

// log size in bytes
#ifndef HS_TM_LOGSIZE
#define HS_TM_LOGSIZE 0xFFFFF
#endif /* HS_TM_LOGSIZE */

// size of the read-set in granules (hint)
#ifndef HS_TM_READSET_HINT
#define HS_TM_READSET_HINT 1024
#endif /* HS_TM_READSET_HINT */

#ifndef HS_TM_WRITESET_HINT
#define HS_TM_WRITESET_HINT 1024
#endif /* HS_TM_READSET_HINT */

// number of combinations with 8 bits
#define HS_TM_LL_UCHAR_SIZE 512

#define HS_TM_WRITE_INPLACE(addr, size, wbuf)        hs_tm_ll_log(addr, size, addr); MEMCPY(addr, wbuf, size)
#define HS_TM_READ_WRITTEN(addr, size, e, rbuf)      MEMCPY(rbuf, addr, size)
#define HS_TM_WRITE_WRITTEN(addr, size, e, wbuf)     MEMCPY(addr, wbuf, size)
#define HS_TM_WRITE_LOG(addr, size, wbuf, log_addr)  MEMCPY(log_addr, addr, size)
#define HS_TM_WRITE_ABORT()                          hs_tm_ll_log_writeback()
#define HS_TM_VALIDATE_RSET()                        /* TODO: check if the version did not changed */
#define HS_TM_WRITE_COMMIT()                         /* empty */

// dst and src must be void* size is size_t
#define MEMCPY(dst, src, size) memcpy(dst, src, size)

// ###################
// datatypes
// ###################

enum HS_TM_LL_LOCK_ { // coded in 2 bits
	HS_TM_LL_FREE = 0, HS_TM_LL_LOCKED = 1
};

enum HS_TM_LL_ABORTS_ {
	HS_TM_LL_A_EXPLICIT = 1, HS_TM_LL_A_LOCKED = 2, HS_TM_LL_A_VALIDATION = 4
};

// this struct must be packed
typedef struct PACKED hs_tm_ll_gran_ {
	unsigned char bytes[HS_TM_GRANULARITY];
} hs_tm_ll_gran_s;

// The read-set is held in a C++ unsorted_map, while the
// write-set is held in a redo-log (for persistent memory).
// In this implementation, one log must be held per thread.
typedef struct hs_tm_ll_wlog_ {
	size_t size;
	// The first char tells how long the next entry is (in Bytes).
	// The pointer must be moved accordingly.
	// Granules larger than 2^8 Bytes must take more than one entry.
	char *ptr;
} hs_tm_ll_wlog_s;

// Use this struct to retrieve the data.
// Be careful, the struct may be beyond the size of the log.
// Also, if it is not correctly packed does not work.
typedef struct PACKED hs_tm_ll_wlog_entry_ {
	unsigned char size;
	void *base_addr;
	// payload
	unsigned char bytes[HS_TM_LL_UCHAR_SIZE];
} hs_tm_ll_wlog_entry_s;

// ###################
// accessible variables
// ###################

extern __thread hs_tm_ll_wlog_s hs_tm_ll_wlog;
extern __thread int           hs_tm_ll_tid;
extern __thread jmp_buf       hs_tm_ll_env;
extern __thread int           hs_tm_ll_in_transaction;

// ###################
// Internal API
// ###################

// ###################
// high-level
// ###################
// Returns if is restart.
// This function must be inline because returning destroys setjmp state
#define hs_tm_ll_begin() ({ \
	int code = -1; \
	if (!hs_tm_ll_in_transaction) { \
		code = SETJMP(hs_tm_ll_env); \
		hs_tm_ll_after_begin(); \
	} \
	hs_tm_ll_in_transaction++; \
	code; \
})
void hs_tm_ll_after_begin();
void hs_tm_ll_commit();
void hs_tm_ll_abort();

void hs_tm_ll_read(void *addr, size_t, void *r_buf);
void hs_tm_ll_write(void *addr, size_t, void *w_buf);

// ###################
// auxiliary functions
// ###################

int hs_tm_ll_validate();

int hs_tm_ll_lock_value(size_t);
size_t hs_tm_ll_log(void *addr, size_t size, void *buffer);
void hs_tm_ll_log_read(size_t entry, void *buffer);
void hs_tm_ll_log_update(size_t entry, size_t size, void *buffer);
size_t hs_tm_ll_log_next_entry(size_t current_entry);
void hs_tm_ll_log_writeback_entry(size_t current_entry);

#ifdef __cplusplus
}
#endif

#endif /* HS_TM_LL_H */
