#include "TestRW.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <string>
#include <sstream>
#include <atomic>
#include <thread>

#include <set>
#include <map>
#include <unordered_set>

using namespace std;

#define NB_INCS 10000

static atomic<int> test_atomic;
static int shared;
static int shared_counter = 0;

static void stress_atomic();

CPPUNIT_TEST_SUITE_REGISTRATION(TestRW);

TestRW::TestRW()  { }
TestRW::~TestRW() { }

void TestRW::setUp()
{
	int i;
	for (i = 0; i < TESTRW_POOL_SIZE; ++i) {
		_granulePool[i] = 0;
	}
	HS_TM_init();
	HS_TM_thr_enter();

	HS_TM_nb_rlock = 0;
	HS_TM_nb_wlock = 0;
	HS_TM_nb_rel_rlock = 0;
	HS_TM_nb_rel_wlock = 0;
	HS_TM_tot_nb_req_sgl = 0;
}

void TestRW::tearDown()
{
	HS_TM_thr_exit();
	HS_TM_destroy();
}

void TestRW::testAtomics()
{
	ostringstream msg;

	test_atomic = 0;

	test_atomic.exchange(1);
	CPPUNIT_ASSERT(test_atomic == 1);

	thread t(stress_atomic);
	test_atomic.exchange(0);
	stress_atomic();
	t.join();

	msg << "Expected " << 2*NB_INCS << " but got " << shared_counter;
	CPPUNIT_ASSERT_MESSAGE(msg.str(), shared_counter == 2*NB_INCS);
	msg.str("");
}

void TestRW::testSets()
{
	ostringstream msg;

	unordered_set<int> uset;
	unordered_set<int>::iterator uset_it;
	set<int> oset;
	set<int>::iterator oset_it;

	// TODO: test sorted map
	uset.insert(1);
	uset.insert(4);
	uset.insert(2);
	uset.insert(3);
	oset.insert(1);
	oset.insert(4);
	oset.insert(2);
	oset.insert(3);

	uset_it = uset.begin();
	oset_it = oset.begin();
	printf("%15s%15s\n", "unordered", "ordered");
	while (uset_it != uset.end()) {
		printf("%15i%15i\n", *uset_it, *oset_it);
		++uset_it;
		++oset_it;
	}
	CPPUNIT_ASSERT(true);
}

void TestRW::testCommit()
{
	ostringstream msg;

	// non-explicit writes are not tracked
	int gran = 0;
	HS_TM_begin();
	gran = 1;
	HS_TM_commit();

	msg << "Expected 1 but got " << gran;
	CPPUNIT_ASSERT_MESSAGE(msg.str(), gran == 1);
	msg.str("");
	msg << "Expected 0 but got " << HS_TM_tot_nb_req_sgl;
	CPPUNIT_ASSERT_MESSAGE(msg.str(), HS_TM_tot_nb_req_sgl == 0);
}

void TestRW::testAbort()
{
	ostringstream msg;

	// non-explicit writes are not tracked
	int gran = 0;
	int restarts = 0;
	restarts += HS_TM_begin() != 0 ? 1 : 0;
	if (gran != 5) {
		gran++;
		HS_TM_abort();
	}
	HS_TM_commit();

	msg << "Expected 5 but got " << restarts;
	CPPUNIT_ASSERT_MESSAGE(msg.str(), restarts == 5);
	msg.str("");
	msg << "Expected 5 but got " << gran;
	CPPUNIT_ASSERT_MESSAGE(msg.str(), gran == 5);
	msg.str("");
	msg << "Expected 0 but got " << HS_TM_tot_nb_req_sgl;
	CPPUNIT_ASSERT_MESSAGE(msg.str(), HS_TM_tot_nb_req_sgl == 0);
}

void TestRW::testReadOnly()
{
	ostringstream msg;

	int res;
	HS_TM_begin();
	HS_TM_read((void*)_granulePool, sizeof(int), (void*)&res);
	HS_TM_commit();

	msg << "Expected 0 but got " << res;
	CPPUNIT_ASSERT_MESSAGE(msg.str(), res == 0);
	msg.str("");
	msg << "Expected 0 but got " << HS_TM_tot_nb_req_sgl;
	CPPUNIT_ASSERT_MESSAGE(msg.str(), HS_TM_tot_nb_req_sgl == 0);
}

void TestRW::testReadMultiple()
{
	ostringstream msg;
	int i, nb_reads = 1024;

	int res[nb_reads], w = 0;
	HS_TM_begin();
	for (i = 0; i < nb_reads; ++i) {
		HS_TM_read((void*)&(_granulePool[i*sizeof(int)]), sizeof(int), (void*)&w);
		res[i] = i + w; // w is 0
	}
	for (i = nb_reads-1; i >= 0; --i) {
		HS_TM_write((void*)&(_granulePool[i*sizeof(int)]), sizeof(int), (void*)&(res[i]));
	}
	HS_TM_commit();

	for (i = 0; i < nb_reads; ++i) {
		int j;
		memcpy(&j, &(_granulePool[i*sizeof(int)]), sizeof(int));
		msg << "Expected " << i << " but got " << j;
		CPPUNIT_ASSERT_MESSAGE(msg.str(), i == j);
		msg.str("");
	}
}

void TestRW::testWriteMultiple()
{
	ostringstream msg;
	int i, nb_reads = 1024;

	int res[nb_reads], w = 0;
	HS_TM_begin();
	for (i = 0; i < nb_reads; ++i) {
		HS_TM_read((void*)&(_granulePool[i*sizeof(int)]), sizeof(int), (void*)&w);
		res[i] = i + w; // w is 0
	}
	for (i = nb_reads-1; i >= 0; --i) {
		HS_TM_write((void*)&(_granulePool[0]), sizeof(int), (void*)&(res[i]));
	}
	HS_TM_commit();

	int j;
	memcpy(&j, &(_granulePool[0]), sizeof(int));
	msg << "Expected 0 but got " << j;
	CPPUNIT_ASSERT_MESSAGE(msg.str(), 0 == j);
	msg.str("");
}

void TestRW::testWriteOnly()
{
	ostringstream msg;

	int res = 5;
	size_t lock = atm_ll_addr_to_lock((void*)_granulePool);
	int state = atm_ll_lock_value(lock);

	// Initially the lock is free
	msg << "Expected " << HS_TM_LL_FREE << " but got " << state;
	CPPUNIT_ASSERT_MESSAGE(msg.str(), state == HS_TM_LL_FREE);
	msg.str("");

	HS_TM_begin();
	HS_TM_write((void*)_granulePool, sizeof(int), (void*)&res);

	state = atm_ll_lock_value(lock);

	#if HS_TM_LAZY_WLOCK_MODE == 0
	// Got the write-lock
	msg << "Expected " << HS_TM_LL_WLOCK << " but got " << state;
	CPPUNIT_ASSERT_MESSAGE(msg.str(), (state & 0b11) == HS_TM_LL_WLOCK);
	msg.str("");
	#else
	msg << "Expected " << HS_TM_LL_RLOCK << " but got " << state;
	CPPUNIT_ASSERT_MESSAGE(msg.str(), (state & 0b11) == HS_TM_LL_RLOCK);
	msg.str("");
	#endif

	HS_TM_commit();

	state = atm_ll_lock_value(lock);

	// Commit frees the write-lock
	msg << "Expected " << HS_TM_LL_FREE << " but got " << state;
	CPPUNIT_ASSERT_MESSAGE(msg.str(), state == HS_TM_LL_FREE);
	msg.str("");
	msg << "Expected 5 but got " << *((int*)_granulePool);
	CPPUNIT_ASSERT_MESSAGE(msg.str(), *((int*)_granulePool) == 5);
	msg.str("");
	msg << "Expected 0 but got " << HS_TM_tot_nb_req_sgl;
	CPPUNIT_ASSERT_MESSAGE(msg.str(), HS_TM_tot_nb_req_sgl == 0);
}

void TestRW::testReadWrite()
{
	ostringstream msg;

	int write = 5, read;
	float writef = 5.0f, readf;
	HS_TM_begin();
	// write/read int
	HS_TM_write((void*)&(_granulePool[0]), sizeof(int), (void*)&write);
	HS_TM_read((void*)&(_granulePool[0]), sizeof(int), (void*)&read);
	// write/read float
	HS_TM_write((void*)&(_granulePool[10]), sizeof(float), (void*)&writef);
	HS_TM_read((void*)&(_granulePool[10]), sizeof(float), (void*)&readf);
	HS_TM_commit();

	msg << "Expected 5 but got " << read;
	CPPUNIT_ASSERT_MESSAGE(msg.str(), read == 5);
	msg.str("");
	msg << "Expected 5.0f but got " << readf;
	CPPUNIT_ASSERT_MESSAGE(msg.str(), readf == 5.0f);
	msg.str("");
	msg << "Expected 0 but got " << HS_TM_tot_nb_req_sgl;
	CPPUNIT_ASSERT_MESSAGE(msg.str(), HS_TM_tot_nb_req_sgl == 0);
}

void TestRW::testResultsAfter()
{
	ostringstream msg;

	int write = 5, read;
	float writef = 1.1234f, readf, readf_after;
	HS_TM_begin();
	// write/read int
	HS_TM_write((void*)&(_granulePool[0]), sizeof(int), (void*)&write);
	HS_TM_read((void*)&(_granulePool[0]), sizeof(int), (void*)&read);

	// On lazy writeback this is unchanged
	// msg << "Expected 0 but got " << (int)(_granulePool[0]);
	// CPPUNIT_ASSERT_MESSAGE(msg.str(), (int)(_granulePool[0]) == 0);

	// write/read float
	HS_TM_write((void*)&(_granulePool[10]), sizeof(float), (void*)&writef);
	HS_TM_read((void*)&(_granulePool[10]), sizeof(float), (void*)&readf);
	HS_TM_commit();

	msg << "Expected 5 but got " << read;
	CPPUNIT_ASSERT_MESSAGE(msg.str(), (int)(_granulePool[0]) == 5);
	CPPUNIT_ASSERT_MESSAGE(msg.str(), (int)(_granulePool[0]) == read);
	msg.str("");
	msg << "Expected 1.1234f but got " << readf;
	memcpy(&readf_after, (void*)&(_granulePool[10]), sizeof(float));
	CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(msg.str(), 1.1234f, readf_after, 0.001f);
}

void TestRW::testGranularity()
{
	ostringstream msg;

	typedef struct some_struct_ {
		double d1;
		float f1;
		int i1;
		double d2;
		long long ll1;
	} some_struct_s;

	float to_write2, to_read2;
	some_struct_s to_write1, to_write3, to_read1, to_read3;

	to_write1.d1 = 1.234;
	to_write1.f1 = 5.678f;
	to_write1.i1 = 2;
	to_write1.d2 = 9.12345679;
	to_write1.ll1 = 1234567890;

	to_write2 = 5.111f;

	memcpy(&to_write3, &to_write1, sizeof(some_struct_s));

	HS_TM_begin();
	HS_TM_write((void*)&(_granulePool[0]), sizeof(some_struct_s), (void*)&to_write1);
	HS_TM_write((void*)&(_granulePool[100]), sizeof(float), (void*)&to_write2);
	HS_TM_write((void*)&(_granulePool[200]), sizeof(some_struct_s), (void*)&to_write3);
	HS_TM_commit();

	memcpy(&to_read1, (void*)&(_granulePool[0]), sizeof(some_struct_s));
	memcpy(&to_read2, (void*)&(_granulePool[100]), sizeof(float));
	memcpy(&to_read3, (void*)&(_granulePool[200]), sizeof(some_struct_s));

	// TODO:
	msg << "Expected 1.234f but got " << to_read1.d1;
	CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(msg.str(), 1.234f, to_read1.d1, 0.001f);
	msg.str("");

	msg << "Expected 1234567890 but got " << to_read1.ll1;
	CPPUNIT_ASSERT_MESSAGE(msg.str(), to_read1.ll1 == 1234567890);
	msg.str("");

	msg << "Expected 5.111f but got " << to_read2;
	CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(msg.str(), 5.111f, to_read2, 0.001f);
	msg.str("");

	msg << "Expected 5.678f but got " << to_read3.f1;
	CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(msg.str(), 5.678f, to_read3.f1, 0.001f);
	msg.str("");

	msg << "Expected 9.12345679 but got " << to_read3.d2;
	CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(msg.str(), 9.12345679, to_read3.d2, 0.001f);
	msg.str("");

}

void TestRW::testMultipleTXs()
{
	ostringstream msg;

	int i, j, k, nb_txs = 1024, nb_reads = 1024;
	int res[nb_reads], w = 0;

	for (j = 0; j < nb_txs; ++j) {
		HS_TM_begin();
		for (i = w; i < nb_reads; ++i) {
			HS_TM_read((void*)&(_granulePool[i*sizeof(int)]),
				sizeof(int), (void*)&(res[i]));
			res[i] = i;
			w++;
		}
		for (i = w-1; i >= 0; --i) {
			HS_TM_write((void*)&(_granulePool[i*sizeof(int)]),
				sizeof(int), (void*)&(res[i]));
			w--;
		}
		HS_TM_commit();
	}

	msg << "Expected " << HS_TM_nb_rlock << " but got " << HS_TM_nb_rel_rlock;
	CPPUNIT_ASSERT_MESSAGE(msg.str(), HS_TM_nb_rlock == HS_TM_nb_rel_rlock);
	msg.str("");
	msg << "Expected " << HS_TM_nb_wlock << " but got " << HS_TM_nb_rel_wlock;
	CPPUNIT_ASSERT_MESSAGE(msg.str(), HS_TM_nb_wlock == HS_TM_nb_rel_wlock);
	msg.str("");
}

void TestRW::testAliasing()
{
	ostringstream msg;

	int res, w = 0;
	HS_TM_begin();
	HS_TM_read((void*)&(_granulePool[0]), sizeof(int), (void*)&res);
	w += 1 + res;
	HS_TM_read((void*)&(_granulePool[0+0xFFFF]), sizeof(int), (void*)&res);
	w += 1 + res;
	HS_TM_read((void*)&(_granulePool[0+2*0xFFFF]), sizeof(int), (void*)&res);
	w += 1 + res;
	HS_TM_write((void*)&(_granulePool[0+0xFFFF]), sizeof(int), (void*)&w);
	HS_TM_write((void*)&(_granulePool[0+2*0xFFFF]), sizeof(int), (void*)&w);
	HS_TM_write((void*)&(_granulePool[0]), sizeof(int), (void*)&w);
	HS_TM_commit();

	msg << "Expected 3 but got " << w;
	CPPUNIT_ASSERT_MESSAGE(msg.str(), w == 3);
}

void TestRW::testSlowDown()
{
	int nb_accesses = 1024;
	int i, r_var, w_var;

	using namespace std::chrono;

	high_resolution_clock::time_point t1, t2, t3;
	duration<double> time_taken, time_commit;

	t1 = high_resolution_clock::now();
	// no instrumentation
	HS_TM_begin();
	for (i = 0; i < nb_accesses; ++i) {
		r_var = w_var;
		r_var++;
		w_var = r_var;
	}
	t3 = high_resolution_clock::now();
	HS_TM_commit();
	t2 = high_resolution_clock::now();

	time_taken = duration_cast<duration<double>>(t2 - t1);
	time_commit = duration_cast<duration<double>>(t2 - t3);
	cout << "Time without instrumentation:    " << time_taken.count()
		<< " (commit: " << time_commit.count() << ")" << endl;

	t1 = high_resolution_clock::now();
	// read only
	HS_TM_begin();
	for (i = 0; i < nb_accesses; ++i) {
		HS_TM_read(&w_var, sizeof(int), &r_var);
		r_var++;
		w_var = r_var;
	}
	t3 = high_resolution_clock::now();
	HS_TM_commit();
	t2 = high_resolution_clock::now();

	time_taken = duration_cast<duration<double>>(t2 - t1);
	time_commit = duration_cast<duration<double>>(t2 - t3);
	cout << "Time with read instrumentation:  " << time_taken.count()
		<< " (commit: " << time_commit.count() << ")" << endl;

	t1 = high_resolution_clock::now();
	// write only
	HS_TM_begin();
	for (i = 0; i < nb_accesses; ++i) {
		r_var = w_var;
		r_var++;
		HS_TM_write(&r_var, sizeof(int), &w_var);
	}
	t3 = high_resolution_clock::now();
	HS_TM_commit();
	t2 = high_resolution_clock::now();

	time_taken = duration_cast<duration<double>>(t2 - t1);
	time_commit = duration_cast<duration<double>>(t2 - t3);
	cout << "Time with write instrumentation: " << time_taken.count()
		<< " (commit: " << time_commit.count() << ")" << endl;

	t1 = high_resolution_clock::now();
	// write and read
	HS_TM_begin();
	for (i = 0; i < nb_accesses; ++i) {
		HS_TM_read(&w_var, sizeof(int), &r_var);
		r_var++;
		HS_TM_write(&r_var, sizeof(int), &w_var);
	}
	t3 = high_resolution_clock::now();
	HS_TM_commit();
	t2 = high_resolution_clock::now();

	time_taken = duration_cast<duration<double>>(t2 - t1);
	cout << "Time with instrumentation:       " << time_taken.count()
		<< " (commit: " << time_commit.count() << ")" << endl;

	CPPUNIT_ASSERT(true);
}

static void stress_atomic()
{
	int i, open = 0, close = 1;

	while(test_atomic.load() == 1) printf("waiting\n");

	// TODO: C++ atomics and fences do not work
	for(i = 0; i < NB_INCS; ++i) {
		// while(!atomic_compare_exchange_strong(&test_atomic, &open, 1));
		while(!__sync_bool_compare_and_swap(&shared, 0, 1));
		shared_counter++;
		atomic_thread_fence(memory_order_seq_cst);
		// while(!atomic_compare_exchange_strong(&test_atomic, &close, 0));
		while(!__sync_bool_compare_and_swap(&shared, 1, 0));
	}
}
