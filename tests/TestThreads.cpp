#include "TestThreads.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <iostream>
#include <string>
#include <sstream>
#include <thread>
#include <chrono>

#define NB_ATTEMPTS 10000

using namespace std;

CPPUNIT_TEST_SUITE_REGISTRATION(TestThreads);

TestThreads::TestThreads()  { }
TestThreads::~TestThreads() { }

static void increment_counter(void *arg, int tid);
static void increment_counter_no_locks(void *arg, int tid);
static void random_access_1thread(void *arg, int tid);
static void random_access(void *arg, int tid);
static void random_access_no_locks(void *arg, int tid);
static void inc_all(void *arg, int tid);
static void init_call(void(*callback)(void*, int), void *arg, int tid);
static unsigned TM_read(unsigned *addr);
static void TM_update(unsigned *addr, unsigned *new_val);

void TestThreads::setUp()
{
	int i;
	for (i = 0; i < NB_GRANS; ++i) {
		_granulePool[i] = 0;
		_sci[i] = 0;
		_scll[i] = 0;
	}
	HS_TM_init();
	HS_TM_thr_enter();

	HS_TM_nb_rlock = 0;
	HS_TM_nb_wlock = 0;
	HS_TM_nb_rel_rlock = 0;
	HS_TM_nb_rel_wlock = 0;
	HS_TM_tot_nb_req_sgl = 0;
}

void TestThreads::tearDown()
{
	HS_TM_thr_exit();
	HS_TM_destroy();
}

void TestThreads::testNoConflict1Thread()
{
	int i;
	increment_counter(&(_scll[0]), 0);
	CPPUNIT_ASSERT(_scll[0] == NB_ATTEMPTS);
}

void TestThreads::testNoConflict16Thread()
{
	ostringstream msg;

	int nb_threads = 16;
	int i;
	thread *threads[nb_threads-1];

	using namespace std::chrono;

	high_resolution_clock::time_point t1, t2, t3, t4;

	t1 = high_resolution_clock::now();
	for (i = 1; i < nb_threads; ++i) {
		threads[i-1] = new thread(init_call, increment_counter_no_locks, &(_scll[i]), i);
	}
	increment_counter_no_locks(&(_scll[0]), 0);
	for (i = 1; i < nb_threads; ++i) {
		threads[i-1]->join();
	}
	t2 = high_resolution_clock::now();

	_scll[0] = 0;
	for (i = 1; i < nb_threads; ++i) {
		_scll[i] = 0;
		delete threads[i-1];
	}

	t3 = high_resolution_clock::now();
	for (i = 1; i < nb_threads; ++i) {
		threads[i-1] = new thread(init_call, increment_counter, &(_scll[i]), i);
	}
	increment_counter(&(_scll[0]), 0);
	for (i = 1; i < nb_threads; ++i) {
		threads[i-1]->join();
	}
	t4 = high_resolution_clock::now();

	duration<double> time_no_locks = duration_cast<duration<double>>(t2 - t1);
	duration<double> time_with_locks = duration_cast<duration<double>>(t4 - t3);

	cout << "Without locks: " << time_no_locks.count() << "s "
		<< "with locks: " << time_with_locks.count() << "s " << endl;

	for (i = 1; i < nb_threads; ++i) {
		delete threads[i-1];
	}

	for (i = 0; i < nb_threads; ++i) {
		msg << "Expected " << NB_ATTEMPTS << " but got " << _scll[i];
		CPPUNIT_ASSERT_MESSAGE(msg.str(), _scll[i] == NB_ATTEMPTS);
		msg.str("");
	}

	msg << "Expected " << HS_TM_nb_rlock << " but got " << HS_TM_nb_rel_rlock;
	CPPUNIT_ASSERT_MESSAGE(msg.str(), HS_TM_nb_rlock == HS_TM_nb_rel_rlock);
	msg.str("");
	msg << "Expected " << HS_TM_nb_wlock << " but got " << HS_TM_nb_rel_wlock;
	CPPUNIT_ASSERT_MESSAGE(msg.str(), HS_TM_nb_wlock == HS_TM_nb_rel_wlock);
	msg.str("");
}

void TestThreads::testRandom1Thread()
{
	ostringstream msg;
	random_access_1thread(&(_scll[0]), 0);
	msg << "Expected 0 but got " << HS_TM_tot_nb_req_sgl;
	CPPUNIT_ASSERT_MESSAGE(msg.str(), HS_TM_tot_nb_req_sgl == 0);
}

void TestThreads::testRandom16Thread()
{
	ostringstream msg;
	int nb_threads = 16;
	int i;
	thread *threads[nb_threads-1];
	unsigned *ptr = (unsigned*)_sci;

	using namespace std::chrono;

	high_resolution_clock::time_point t1, t2, t3, t4;

	t1 = high_resolution_clock::now();
	for (i = 1; i < nb_threads; ++i) {
		threads[i-1] = new thread(init_call, random_access_no_locks, _sci, i);
	}
	random_access_no_locks(_sci, 0);
	for (i = 1; i < nb_threads; ++i) {
		threads[i-1]->join();
	}
	t2 = high_resolution_clock::now();

	for (i = 0; i < NB_GRANS; ++i) {
		ptr[i] = 0;
	}

	for (i = 1; i < nb_threads; ++i) {
		delete threads[i-1];
	}

	HS_TM_nb_aborts = 0;
	HS_TM_tot_nb_aborts = 0;

	t3 = high_resolution_clock::now();
	for (i = 1; i < nb_threads; ++i) {
		threads[i-1] = new thread(init_call, random_access, ptr, i);
	}
	random_access(ptr, 0);
	for (i = 1; i < nb_threads; ++i) {
		threads[i-1]->join();
	}
	t4 = high_resolution_clock::now();

	duration<double> time_no_locks = duration_cast<duration<double>>(t2 - t1);
	duration<double> time_with_locks = duration_cast<duration<double>>(t4 - t3);

	cout << "Without locks: " << time_no_locks.count() << "s" << endl
			 << "With locks:    " << time_with_locks.count() << "s" << endl
			 << "(" << HS_TM_tot_nb_aborts << " aborts; " << HS_TM_tot_nb_commits
			 << " commits)" << endl;

	for (i = 1; i < nb_threads; ++i) {
		delete threads[i-1];
	}

	unsigned long long sum = 0;
	for (i = 0; i < NB_GRANS; ++i) {
		sum += ptr[i];
	}

	msg << "Expected " << nb_threads * 4 * NB_ATTEMPTS << " but got " << sum;
	CPPUNIT_ASSERT_MESSAGE(msg.str(), nb_threads * 4 * NB_ATTEMPTS == sum);
	msg.str("");

	// not working with sort locks
	// msg << "Expected " << HS_TM_nb_rlock << " but got " << HS_TM_nb_rel_rlock;
	// CPPUNIT_ASSERT_MESSAGE(msg.str(), HS_TM_nb_rlock == HS_TM_nb_rel_rlock);
	// msg.str("");
	msg << "Expected " << HS_TM_nb_wlock << " but got " << HS_TM_nb_rel_wlock;
	CPPUNIT_ASSERT_MESSAGE(msg.str(), HS_TM_nb_wlock == HS_TM_nb_rel_wlock);
	msg.str("");
}

void TestThreads::testCounter()
{
	ostringstream msg;
	int nb_threads = 16;
	int i;
	thread *threads[nb_threads-1];

	for (i = 1; i < nb_threads; ++i) {
		threads[i-1] = new thread(init_call, inc_all, _sci, i);
	}
	inc_all(_sci, 0);
	for (i = 1; i < nb_threads; ++i) {
		threads[i-1]->join();
	}

	for (i = 1; i < nb_threads; ++i) {
		delete threads[i-1];
	}

	int nb_g = 8;
	unsigned max = 0;

	for (i = 0; i < nb_g; ++i) {
		if ((unsigned)_sci[i*4] > max) max = (unsigned)_sci[i*4];
	}

	msg << "Expected " << nb_threads * NB_ATTEMPTS << " but got " << max;
	CPPUNIT_ASSERT_MESSAGE(msg.str(), nb_threads * NB_ATTEMPTS == max);
	msg.str("");

	msg << "Expected " << HS_TM_nb_rlock << " but got " << HS_TM_nb_rel_rlock;
	CPPUNIT_ASSERT_MESSAGE(msg.str(), HS_TM_nb_rlock == HS_TM_nb_rel_rlock);
	msg.str("");
	msg << "Expected " << HS_TM_nb_wlock << " but got " << HS_TM_nb_rel_wlock;
	CPPUNIT_ASSERT_MESSAGE(msg.str(), HS_TM_nb_wlock == HS_TM_nb_rel_wlock);
	msg.str("");
}

static void increment_counter(void *arg, int tid)
{
	long long *target = (long long*)arg;
	int i;

	for (i = 0; i < NB_ATTEMPTS; ++i) {
		long long tmp;
		HS_TM_begin();
		HS_TM_read(target, sizeof(long long), &tmp);
		tmp += 1;
		HS_TM_write(target, sizeof(long long), &tmp);
		HS_TM_commit();
	}
}

static void increment_counter_no_locks(void *arg, int tid)
{
	long long *target = (long long*)arg;
	int i;

	for (i = 0; i < NB_ATTEMPTS; ++i) {
		long long tmp;
		tmp = *target;
		tmp += 1;
		*target = tmp;
	}
}

static void random_access_1thread(void *arg, int tid)
{
	// generates lots of false conflicts
	unsigned char *granulePool = (unsigned char*)arg;
	int i;
	unsigned seed = 1234 * tid;
	ostringstream msg;

	for (i = 0; i < NB_ATTEMPTS; ++i) {
		unsigned rand1, rand2, rand3, rand4;
		unsigned char inc1, inc2, inc3, inc4;

		rand1 = rand_r(&seed) % (NB_GRANS / 16);
		do {
			rand2 = rand_r(&seed) % (NB_GRANS / 16);
		} while (rand2 == rand1);
		do {
			rand3 = rand_r(&seed) % (NB_GRANS / 16);
		} while (rand3 == rand1 || rand3 == rand2);
		do {
			rand4 = rand_r(&seed) % (NB_GRANS / 16);
		} while (rand4 == rand1 || rand4 == rand2 || rand4 == rand3);

		HS_TM_begin();
		HS_TM_read(&(granulePool[rand1]), sizeof(unsigned char), &inc1);
		HS_TM_read(&(granulePool[rand2]), sizeof(unsigned char), &inc2);
		HS_TM_read(&(granulePool[rand3]), sizeof(unsigned char), &inc3);
		HS_TM_read(&(granulePool[rand4]), sizeof(unsigned char), &inc4);

		inc1 += 1;
		inc2 += inc1;
		inc3 += inc2;
		inc4 += inc3;

		HS_TM_write(&(granulePool[rand1]), sizeof(unsigned char), &inc1);
		HS_TM_write(&(granulePool[rand2]), sizeof(unsigned char), &inc2);
		HS_TM_write(&(granulePool[rand3]), sizeof(unsigned char), &inc3);
		HS_TM_write(&(granulePool[rand4]), sizeof(unsigned char), &inc4);

		HS_TM_commit();

		msg << "Expected " << (int)inc2 << " but got " << (int)(granulePool[rand2]);
		CPPUNIT_ASSERT_MESSAGE(msg.str(), granulePool[rand2] == inc2);
		msg.str("");
		msg << "Expected " << (int)inc4 << " but got " << (int)(granulePool[rand4]);
		CPPUNIT_ASSERT_MESSAGE(msg.str(), granulePool[rand4] == inc4);
		msg.str("");
	}
}

static void random_access(void *arg, int tid)
{
	// generates lots of false conflicts
	unsigned *granulePool = (unsigned*)arg;
	int i;
	unsigned seed = 1234 * tid;
	unsigned sum = 0, sum_tmp, sum2 = 0;

	for (i = 0; i < NB_ATTEMPTS; ++i) {
		unsigned rand1, rand2, rand3, rand4;
		unsigned inc1, inc2, inc3, inc4;

		sum2 += 4;

		rand1 = rand_r(&seed) % NB_GRANS;
		do {
			rand2 = rand_r(&seed) % NB_GRANS;
		} while (rand2 == rand1);
		do {
			rand3 = rand_r(&seed) % NB_GRANS;
		} while (rand3 == rand1 || rand3 == rand2);
		do {
			rand4 = rand_r(&seed) % NB_GRANS;
		} while (rand4 == rand1 || rand4 == rand2 || rand4 == rand3);

		HS_TM_begin();
		inc1 = TM_read(&(granulePool[rand1]));
		inc2 = TM_read(&(granulePool[rand2]));
		inc3 = TM_read(&(granulePool[rand3]));
		inc4 = TM_read(&(granulePool[rand4]));

		// the values are not roll-backed!
		sum_tmp = TM_read(&sum);
		inc1 += 1;
		inc2 += 1;
		inc3 += 1;
		inc4 += 1;
		sum_tmp += 1 + 1 + 1 + 1;
		TM_update(&sum, &sum_tmp);
		// each transactions adds 10 to the total
		TM_update(&(granulePool[rand2]), &inc2);
		TM_update(&(granulePool[rand3]), &inc3);
		TM_update(&(granulePool[rand4]), &inc4);
		TM_update(&(granulePool[rand1]), &inc1);
		HS_TM_commit();
	}
}

static void inc_all(void *arg, int tid)
{
	// granule 0 is always incremented
	// then granules (tid*4+1), (tid*4+2), (tid*4+3), (tid*4+4)
	unsigned *g_pool = (unsigned*)arg;
	int i, j, nb_g = 8;
	unsigned seed = 1234 * tid;
	unsigned min, max;
	unsigned g[nb_g];

	for (i = 0; i < NB_ATTEMPTS; ++i) {

		HS_TM_begin();
		min = 0, max = 0;

		for (j = 0; j < nb_g; ++j) {
			HS_TM_read(&(g_pool[j * 4]), sizeof(unsigned), &(g[j]));
			if (g[j] < g[min]) {
				min = j;
			}
			if (g[j] > g[max]) {
				max = j;
			}
		}
		g[min] = g[max] + 1;
		HS_TM_write(&(g_pool[max * 4]), sizeof(unsigned), &(g[min]));
		HS_TM_write(&(g_pool[min * 4]), sizeof(unsigned), &(g[max]));

		HS_TM_commit();
	}
}

static void random_access_no_locks(void *arg, int tid)
{
	// generates lots of false conflicts
	unsigned char *granulePool = (unsigned char*)arg;
	int i;
	unsigned seed = 1234 * tid;

	for (i = 0; i < NB_ATTEMPTS; ++i) {
		unsigned rand1, rand2, rand3, rand4;
		unsigned char inc1, inc2, inc3, inc4;

		rand1 = rand_r(&seed) % NB_GRANS;
		do {
			rand2 = rand_r(&seed) % NB_GRANS;
		} while (rand2 == rand1);
		do {
			rand3 = rand_r(&seed) % NB_GRANS;
		} while (rand3 == rand1 || rand3 == rand2);
		do {
			rand4 = rand_r(&seed) % NB_GRANS;
		} while (rand4 == rand1 || rand4 == rand2 || rand4 == rand3);

		inc1 = granulePool[rand1];
		inc2 = granulePool[rand2];
		inc3 = granulePool[rand3];
		inc4 = granulePool[rand4];
		inc1 += 1;
		inc2 += inc1;
		inc3 += inc2;
		inc4 += inc3;
		granulePool[rand1] = inc1;
		granulePool[rand2] = inc2;
		granulePool[rand3] = inc3;
		granulePool[rand4] = inc4;
	}
}

static void init_call(void(*callback)(void*, int), void *arg, int tid)
{
	HS_TM_thr_enter();
	callback(arg, tid);
	HS_TM_thr_exit();
}

static unsigned TM_read(unsigned *addr)
{
	unsigned res;
	HS_TM_read(addr, sizeof(unsigned), &res);
	return res;
}

static void TM_update(unsigned *addr, unsigned *new_val)
{
	HS_TM_write(addr, sizeof(unsigned), new_val);
}
