#ifndef TESTRW_H
#define TESTRW_H

#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/ui/text/TextTestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/XmlOutputter.h>
#include <netinet/in.h>
#include <chrono>

#include "hs_tm.h"

#define TESTRW_POOL_SIZE 0x2FFFF

class TestRW : public CPPUNIT_NS::TestFixture
{
	CPPUNIT_TEST_SUITE(TestRW);

	CPPUNIT_TEST(testAtomics);
	CPPUNIT_TEST(testSets);
	CPPUNIT_TEST(testCommit);
	CPPUNIT_TEST(testAbort);
	CPPUNIT_TEST(testReadOnly);
	CPPUNIT_TEST(testReadMultiple);
	CPPUNIT_TEST(testWriteOnly);
	CPPUNIT_TEST(testWriteMultiple);
	CPPUNIT_TEST(testReadWrite);
	CPPUNIT_TEST(testResultsAfter);
	CPPUNIT_TEST(testGranularity);
	CPPUNIT_TEST(testMultipleTXs);
	CPPUNIT_TEST(testAliasing);
	CPPUNIT_TEST(testSlowDown);

	CPPUNIT_TEST_SUITE_END();

public:
	TestRW();
	virtual ~TestRW();
	void setUp();
	void tearDown();

private:

	char _granulePool[TESTRW_POOL_SIZE];

	void testAtomics();
	void testSets();
	void testCommit();
	void testAbort();

	void testReadOnly();
	void testReadMultiple();
	void testWriteOnly();
	void testWriteMultiple();
	void testReadWrite();

	void testResultsAfter();
	void testGranularity();
	void testMultipleTXs();
	void testAliasing();
	void testSlowDown();
} ;

#endif /* TESTRW_H */
