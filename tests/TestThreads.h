#ifndef TESTTHREADS_H
#define TESTTHREADS_H

#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/ui/text/TextTestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/XmlOutputter.h>
#include <netinet/in.h>

#include "hs_tm.h"

#define NB_GRANS 8192

class TestThreads : public CPPUNIT_NS::TestFixture
{
	CPPUNIT_TEST_SUITE(TestThreads);

	CPPUNIT_TEST(testNoConflict1Thread);
	CPPUNIT_TEST(testNoConflict16Thread);
	CPPUNIT_TEST(testRandom1Thread);
	CPPUNIT_TEST(testRandom16Thread);
	CPPUNIT_TEST(testCounter);

	CPPUNIT_TEST_SUITE_END();

public:
	TestThreads();
	virtual ~TestThreads();
	void setUp();
	void tearDown();

private:

	char _granulePool[NB_GRANS];
	int _sci[NB_GRANS];
	long long _scll[NB_GRANS];

	void testNoConflict1Thread();
	void testNoConflict16Thread();
	void testRandom1Thread();
	void testRandom16Thread();
	void testCounter();

} ;

#endif /* TESTTHREADS_H */
