#include "hs_tm.h"

#include <stdint.h>
#include <assert.h>
#include <string.h>

#include <mutex>
#include <set>
#include <unordered_map>
#include <vector>
#include <list>
#include <thread>

#define LOAD(addr) \
	__atomic_load_n(addr, __ATOMIC_ACQUIRE) \
//
#define STORE(addr, val) \
	__atomic_store_n(addr, val, __ATOMIC_RELEASE) \
//
#define ATOMIC_OR(addr, val) \
	__atomic_fetch_or(addr, val, __ATOMIC_ACQ_REL) \
//
#define ATOMIC_AND(addr, val) \
	__atomic_fetch_and(addr, val, __ATOMIC_ACQ_REL) \
//
#define CAS_WEAK(addr, oldv, newv) ({ \
	__sync_bool_compare_and_swap(addr, oldv, newv); \
})
// #define CAS_WEAK(addr, oldv, newv) ({ int res = *addr == oldv; *addr = newv; res; })
#define CAS_STRONG(addr, oldv, newv) ({ \
	CAS_WEAK(addr, oldv, newv); \
})
// #define CAS_STRONG(addr, oldv, newv) ({ int res = *addr == oldv; *addr = newv; res; })
// synchronizes all caches (e.g., before releasing the locks)
#define MEM_FENCE() __sync_synchronize()
// #define MEM_FENCE()

#define THR_YIELD() this_thread::yield()

#define IS_LOCKED(addr) ({ \
	uint32_t idx = (((uintptr_t)(addr)>>(HS_TM_LOG2GRANULARITY+LOCK_MASK_BITS))&HS_TM_LOCKTABLESIZE); \
	uint32_t offset = ((uintptr_t)(addr)>>(HS_TM_LOG2GRANULARITY))&LOCK_MASK; \
	uint32_t mask = 1<<offset; \
	uint32_t lock = LOAD(&(hs_tm_ll_lock_table[idx])); \
	(lock&mask)>>offset; \
})\
//
#define SET_LOCKED(addr) \
	uint32_t idx = (((uintptr_t)(addr)>>(HS_TM_LOG2GRANULARITY+LOCK_MASK_BITS))&HS_TM_LOCKTABLESIZE); \
	uint32_t offset = ((uintptr_t)(addr)>>(HS_TM_LOG2GRANULARITY))&LOCK_MASK; \
	uint32_t mask = 1<<offset; \
	ATOMIC_OR(&(hs_tm_ll_lock_table[idx]), mask); \
//
#define SET_UNLOCKED(addr) \
	uint32_t idx = (((uintptr_t)(addr)>>(HS_TM_LOG2GRANULARITY+LOCK_MASK_BITS))&HS_TM_LOCKTABLESIZE); \
	uint32_t offset = ((uintptr_t)(addr)>>(HS_TM_LOG2GRANULARITY))&LOCK_MASK; \
	uint32_t mask = 1<<offset; \
	ATOMIC_AND(&(hs_tm_ll_lock_table[idx]), ~mask); \
//

// TODO: redo all of these
#define IS_IN_RSET(lock_id) \
	(read_set.find(lock_id) != read_set.end()) \
//
#define IS_IN_WSET(lock_id) \
	(write_set.find(lock_id) != write_set.end()) \
//
#define FIND_IN_RSET(lock_id) \
	set<size_t>::iterator find_r_it; \
	bool is_found_rlock; \
	find_r_it = read_set.find(lock_id); \
	is_found_rlock = (find_r_it != read_set.end()) \
//
#define FIND_IN_WSET(lock_id) \
	set<size_t>::iterator find_w_it; \
	bool is_found_wlock; \
	find_w_it = write_set.find(lock_id); \
	is_found_wlock = (find_w_it != write_set.end()) \
//
#define FIND_IN_WLOG(addr) \
	unordered_map<void*,size_t>::iterator find_wlog_it; \
	bool is_found_wlog; \
	find_wlog_it = write_log.find(addr); \
	is_found_wlog = (find_wlog_it != write_log.end()) \
//

using namespace std;

// #############################################
// external variables
uint32_t hs_tm_ll_lock_table[HS_TM_LOCKTABLESIZE+1];
#define LOCK_MASK      0x1F
#define LOCK_MASK_BITS 5

__thread hs_tm_ll_wlog_s hs_tm_ll_wlog;
__thread int           hs_tm_ll_tid;
__thread jmp_buf       hs_tm_ll_env;
__thread int           hs_tm_ll_in_transaction;
__thread unsigned long enter_clock;

// statistics
unsigned long long          HS_TM_nb_reads;
unsigned long long          HS_TM_nb_writes;
unsigned long long          HS_TM_nb_htm_aborts;
unsigned long long          HS_TM_nb_htm_commits;
unsigned long long          HS_TM_nb_cas;
unsigned long long          HS_TM_nb_aborts;
unsigned long long          HS_TM_nb_commits;

__thread unsigned long long HS_TM_tot_nb_reads;
__thread unsigned long long HS_TM_tot_nb_writes;
__thread unsigned long long HS_TM_tot_nb_htm_aborts;
__thread unsigned long long HS_TM_tot_nb_htm_commits;
__thread unsigned long long HS_TM_tot_nb_cas;
__thread unsigned long long HS_TM_tot_nb_aborts;
__thread unsigned long long HS_TM_tot_nb_commits;
// #############################################

// #############################################
// local variables
static int tid_counter = 0;
static mutex mtx;
static unsigned long global_clock = 0;

static thread_local set<size_t> read_set; // <lock_id>
static thread_local set<size_t> write_set; // <lock_id>
static thread_local set<size_t> req_w_set; // <lock_id>
static thread_local unordered_map<void*,size_t> write_log; // <base_addr, pos_lock>
static thread_local unsigned nb_retries;
// #############################################

// #############################################
// local functions
static inline void* addr_gran(void *base_addr);
static inline void break_addrs(void *base_addr, size_t size, vector<void*> &addrs);
static inline void wlock(void *base_addr, size_t size);
static inline void abort_code(int code);
static inline void release();
// #############################################

// #############################################
// API implementation

void HS_TM_init()
{
	// hs_tm_ll_lock_table.ptr is already init to 0
	HS_TM_nb_reads       = 0;
	HS_TM_nb_writes      = 0;
	HS_TM_nb_htm_aborts  = 0;
	HS_TM_nb_htm_commits = 0;
	HS_TM_nb_cas         = 0;
	HS_TM_nb_aborts      = 0;
	HS_TM_nb_commits     = 0;
	memset(hs_tm_ll_lock_table, 0, HS_TM_LOCKTABLESIZE*sizeof(uint32_t));
}

void HS_TM_destroy()
{
	// empty
}

void HS_TM_thr_enter()
{
	mtx.lock();
	if (!hs_tm_ll_wlog.ptr)
		hs_tm_ll_wlog.ptr = (char*)malloc(HS_TM_LOGSIZE * sizeof(char));
	write_log.reserve(HS_TM_WRITESET_HINT);
	hs_tm_ll_tid = tid_counter++;
	mtx.unlock();
}

void HS_TM_thr_exit()
{
	mtx.lock();
	free(hs_tm_ll_wlog.ptr);
	hs_tm_ll_wlog.ptr = nullptr;
	HS_TM_tot_nb_reads       += HS_TM_nb_reads;
	HS_TM_tot_nb_writes      += HS_TM_nb_writes;
	HS_TM_tot_nb_htm_aborts  += HS_TM_nb_htm_aborts;
	HS_TM_tot_nb_htm_commits += HS_TM_nb_htm_commits;
	HS_TM_tot_nb_cas         += HS_TM_nb_cas;
	HS_TM_tot_nb_aborts      += HS_TM_nb_aborts;
	HS_TM_tot_nb_commits     += HS_TM_nb_commits;
	tid_counter--;
	mtx.unlock();
}
// #############################################

// #############################################
// restricted API implementation

void hs_tm_ll_after_begin()
{
	enter_clock = LOAD(&global_clock);
	nb_retries = 0;
}

void hs_tm_ll_commit()
{
	// TODO:
	//  1) sort write set (some rule, say per address)
	//  2) lock write-set (wait on locked)
	//  3) validate read-set (go to 4 on abort)
	//  4) unlock
	if (hs_tm_ll_in_transaction == 1) {
		HS_TM_nb_commits++;
		HS_TM_VALIDATE_RSET(); // may abort here
		HS_TM_WRITE_COMMIT(); // apply the changes
		release();
	}
	hs_tm_ll_in_transaction--;
}

void hs_tm_ll_abort()
{
	// retries
	abort_code(HS_TM_LL_A_EXPLICIT);
}



void hs_tm_ll_read(void *addr, size_t size, void *r_buf)
{
	uint8_t buffer[HS_TM_LL_UCHAR_SIZE];

	assert(size < HS_TM_LL_UCHAR_SIZE); // TODO: support more than 512B chuncks
	assert(addr != NULL);
	assert(r_buf != NULL);

	FIND_IN_WLOG(addr); // find_wlog_it, is_found_wlog

	if (is_found_wlog) {
		// read a previously written value
		hs_tm_ll_log_read(find_wlog_it->second, r_buf);
		return; // done
	}

	// TODO:
	//  1) if is locked abort
	//  2) if enter_clock != global_clock validate
	//    2.a) if valid enter_clock = global_clock
	//    2.b) else abort due to validation

	// TODO: read twice, if value is not the same, then abort

	MEMCPY(r_buf, addr, size); // copies the result into the given buffer
	__atomic_thread_fence(__ATOMIC_ACQUIRE);
	MEMCPY((void*)buffer, addr, size);
	
	if (memcmp(r_buf, (void*)buffer, size) != 0) {
		// TODO: abort due to validation
	}
}

void hs_tm_ll_write(void *addr, size_t size, void *w_buf)
{
	assert(size < HS_TM_LL_UCHAR_SIZE); // TODO: support more than 512B chuncks
	assert(addr != NULL);
	assert(w_buf != NULL);

	FIND_IN_WLOG(addr); // find_wlog_it, is_found_wlog
	size_t entry;

	if (is_found_wlog) {
		// already locked, update the log
		hs_tm_ll_log_update(find_wlog_it->second, size, w_buf);
		return; // done
	}

	// wlock in the end
	write_set.insert(hs_tm_ll_addr_to_lock(addr));
	hs_tm_ll_log(addr, size, w_buf);

}

int hs_tm_ll_validate()
{
	// TODO: check 
	//  1) addresses in read-set are not locked
	//  2) values in read-set are still fresh
	return 1;
}


size_t hs_tm_ll_log_next_entry(size_t e)
{
	size_t res;
	void *buf = &(hs_tm_ll_wlog.ptr[e]);
	hs_tm_ll_wlog_entry_s *entry = (hs_tm_ll_wlog_entry_s*)buf;
	res = e + entry->size + sizeof(char) + sizeof(void*);
	return res;
}

size_t hs_tm_ll_log(void *addr, size_t size, void *buffer)
{
	assert(addr != NULL);

	size_t i = hs_tm_ll_wlog.size;
	size_t res = hs_tm_ll_wlog.size;
	void *buf = &(hs_tm_ll_wlog.ptr[i]);
	hs_tm_ll_wlog_entry_s *entry = (hs_tm_ll_wlog_entry_s*)buf;

	entry->base_addr = addr;
	entry->size = (unsigned char)size;
	// #if HS_TM_LAZY_WLOCK_MODE == 0
	// 	HS_TM_WRITE_LOG(addr, size, buffer, (void*)entry->bytes);
	// #else
	memcpy(entry->bytes, buffer, size);
	// #endif
	hs_tm_ll_wlog.size += size + sizeof(unsigned char) + sizeof(void*);
	write_log.insert(make_pair(addr, res)); // log occupation info

	// #if HS_TM_LAZY_WLOCK_MODE == 0
	// 	assert(LOCK_STATE(hs_tm_ll_addr_to_lock(entry->base_addr)) == HS_TM_LL_WLOCK);
	// #endif

	return res;
}

void hs_tm_ll_log_read(size_t e, void *buffer)
{
	void *buf = &(hs_tm_ll_wlog.ptr[e]);
	hs_tm_ll_wlog_entry_s *entry = (hs_tm_ll_wlog_entry_s*)buf;
	MEMCPY(buffer, (void*)entry->bytes, (size_t)entry->size);
}

void hs_tm_ll_log_update(size_t e, size_t size, void *buffer)
{
	void *buf = &(hs_tm_ll_wlog.ptr[e]);
	hs_tm_ll_wlog_entry_s *entry = (hs_tm_ll_wlog_entry_s*)buf;
	MEMCPY((void*)entry->bytes, buffer, (size_t)entry->size);
}

void hs_tm_ll_log_writeback_entry(size_t e)
{
	void *buf = &(hs_tm_ll_wlog.ptr[e]);
	hs_tm_ll_wlog_entry_s *entry = (hs_tm_ll_wlog_entry_s*)buf;
	MEMCPY(entry->base_addr, (void*)entry->bytes, (size_t)entry->size);
}
// #############################################

// #############################################
// local functions implementation
static inline void* addr_gran(void *base_addr)
{
	uintptr_t res = (uintptr_t)base_addr;
	res >>= HS_TM_LOG2GRANULARITY;
	res <<= HS_TM_LOG2GRANULARITY; // clean the least significant bits
	return (void*)res;
}

static inline void break_addrs(void *base_addr, size_t size, vector<void*> &addrs)
{
	int i;
	int padding = size % HS_TM_GRANULARITY;
	int nb_gran = size / HS_TM_GRANULARITY;
	hs_tm_ll_gran_s *granules = (hs_tm_ll_gran_s*)base_addr;

	addrs.clear();
	addrs.reserve(nb_gran + 1);

	for (i = 0; i < nb_gran; ++i) {
		addrs.push_back((void*)&(granules[i]));
	}
	if (padding) {
	 	// TODO: locking more than the size of the object
		addrs.push_back((void*)&(granules[i]));
	}
}

static inline void wlock(void *base_addr, size_t size)
{
	vector<void*> addrs;
	vector<void*>::iterator it;

	break_addrs(base_addr, size, addrs);
	for (it = addrs.begin(); it != addrs.end(); ++it) {
		size_t lock_id = hs_tm_ll_addr_to_lock(*it);
		if (!hs_tm_ll_wlock(lock_id, 1)) abort_code(HS_TM_LL_LOCKED);
	}
}

static inline void abort_code(int code)
{
	hs_tm_ll_in_transaction--; // increment again in begin
	HS_TM_nb_aborts++;
	HS_TM_WRITE_ABORT();
	release();
	MEM_FENCE();
	if (code == HS_TM_LL_A_LOCKED) THR_YIELD();
	LONGJMP(hs_tm_ll_env, code);
}

static inline void release()
{
	hs_tm_ll_wlog.size = 0;
	hs_tm_ll_unlock_all();
	read_set.clear();
	write_set.clear();
	write_log.clear();
}
// #############################################
